package horo

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"

	"gitlab.com/horo-go/errorr"
)

type Context struct {
	Writer  http.ResponseWriter
	Request *http.Request
}

func (c *Context) Authentication() *Authentication {
	au, ok := c.Request.Context().Value(AuthKey).(*Authentication)
	if !ok {
		panic(errorr.ErrNoAuthentication)
	}
	return au
}

func (c *Context) mustAuthenticated() {
	if !c.Authentication().Authenticated {
		panic(errorr.ErrNotAuthenticated)
	}
}

func (c *Context) Subject() string {
	c.mustAuthenticated()
	return c.Authentication().Subject
}

func (c *Context) SubjectInt64() int64 {
	c.mustAuthenticated()
	sub, err := strconv.ParseInt(c.Authentication().Subject, 10, 64)
	if err != nil {
		panic(errorr.NonRecovery("authentication subject is not number"))
	}
	return sub
}

func (c *Context) AccessToken() string {
	c.mustAuthenticated()
	return c.Authentication().AccessToken
}

func (c *Context) ParamInt64(name string) int64 {
	params := c.Request.Context().Value(ParamsKey).(Params)
	value, index := params.Get(name)
	if index == -1 {
		panic(errorr.ErrNoPrams)
	}
	i, ok := value.(int64)
	if !ok {
		panic(errorr.ErrWrongParamType)
	}
	return i
}

func (c *Context) BadRequest() {
	http.Error(c.Writer, "bad request", http.StatusBadRequest)
}

func (c *Context) WriteHeader(code int) {
	c.Writer.WriteHeader(code)
}

func (c *Context) BindJSON(obj interface{}) error {
	if err := json.NewDecoder(c.Request.Body).Decode(obj); err != nil {
		return err
	}
	return nil
}

// String writes the given string into response body
func (c *Context) String(code int, format string, values ...interface{}) {
	c.Writer.WriteHeader(code)
	writeContentType(c.Writer, []string{"text/plain; charset=utf-8"})
	if !bodyAllowedForStatus(code) {
		return
	}

	if len(values) > 0 {
		fmt.Fprintf(c.Writer, format, values...)
	} else {
		io.WriteString(c.Writer, format)
	}
}

func (c *Context) JSON(code int, obj interface{}) {
	c.Writer.WriteHeader(code)
	writeContentType(c.Writer, []string{"application/json; charset=utf-8"})
	if !bodyAllowedForStatus(code) {
		return
	}

	if jsonBytes, err := json.Marshal(obj); err != nil {
		panic(err)
	} else {
		c.Writer.Write(jsonBytes)
	}
}

// bodyAllowedForStatus is a copy of http.bodyAllowedForStatus non-exported function.
func bodyAllowedForStatus(status int) bool {
	switch {
	case status >= 100 && status <= 199:
		return false
	case status == 204:
		return false
	case status == 304:
		return false
	}
	return true
}

func writeContentType(w http.ResponseWriter, value []string) {
	header := w.Header()
	if val := header["Content-Type"]; len(val) == 0 {
		header["Content-Type"] = value
	}
}
