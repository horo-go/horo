package horo

import (
	"net/http"
)

type Router interface {
	Route
	Group(string, ...Handler) *RouterGroup
	With(...Handler) *RouterGroup
}

type Route interface {
	Use(...Handler) Route

	GET(string, ContextHandleFunc) Route
	POST(string, ContextHandleFunc) Route
	DELETE(string, ContextHandleFunc) Route
	PATCH(string, ContextHandleFunc) Route
	PUT(string, ContextHandleFunc) Route
	OPTIONS(string, ContextHandleFunc) Route
	HEAD(string, ContextHandleFunc) Route
}

// RouterGroup is used internally to configure router, a RouterGroup is associated with a prefix
// and an array of handlers (middleware)
type RouterGroup struct {
	chain    HandlerChain
	basePath string
	engine   *Engine
	root     bool
}

// Use add middleware to the group
func (group *RouterGroup) Use(middleware ...Handler) Route {
	group.chain = append(group.chain, middleware...)
	return group.returnObj()
}

// Group creates a new router group.
func (group *RouterGroup) Group(relativePath string, chain ...Handler) *RouterGroup {
	return &RouterGroup{
		chain:    group.combineChain(chain),
		basePath: group.calculateAbsolutePath(relativePath),
		engine:   group.engine,
	}
}

// With creates a new router group with common middleware.
func (group *RouterGroup) With(chain ...Handler) *RouterGroup {
	return group.Group("", chain...)
}

func (group *RouterGroup) handle(httpMethod, relativePath string, h http.Handler) Route {
	absolutePath := group.calculateAbsolutePath(relativePath)
	group.engine.addRoute(httpMethod, absolutePath, group.chain, h)
	return group.returnObj()
}

func (group *RouterGroup) GET(relativePath string, handlerFunc ContextHandleFunc) Route {
	return group.handle("GET", relativePath, handlerFunc)
}

func (group *RouterGroup) POST(relativePath string, handlerFunc ContextHandleFunc) Route {
	return group.handle("POST", relativePath, handlerFunc)
}

func (group *RouterGroup) DELETE(relativePath string, handlerFunc ContextHandleFunc) Route {
	return group.handle("DELETE", relativePath, handlerFunc)
}

func (group *RouterGroup) PATCH(relativePath string, handlerFunc ContextHandleFunc) Route {
	return group.handle("PATCH", relativePath, handlerFunc)
}

func (group *RouterGroup) PUT(relativePath string, handlerFunc ContextHandleFunc) Route {
	return group.handle("PUT", relativePath, handlerFunc)
}

func (group *RouterGroup) OPTIONS(relativePath string, handlerFunc ContextHandleFunc) Route {
	return group.handle("OPTIONS", relativePath, handlerFunc)
}

func (group *RouterGroup) HEAD(relativePath string, handlerFunc ContextHandleFunc) Route {
	return group.handle("HEAD", relativePath, handlerFunc)
}

func (group *RouterGroup) combineChain(chain HandlerChain) HandlerChain {
	finalSize := len(group.chain) + len(chain)
	mergedChain := make(HandlerChain, finalSize)
	copy(mergedChain, group.chain)
	copy(mergedChain[len(group.chain):], chain)
	return mergedChain
}

func (group *RouterGroup) calculateAbsolutePath(relativePath string) string {
	return joinPaths(group.basePath, relativePath)
}

func (group *RouterGroup) returnObj() Route {
	if group.root {
		return group.engine
	}
	return group
}
