package horo

import (
	"testing"

	"gitlab.com/lattetalk/lattetalk/vendor/github.com/stretchr/testify/assert"
	"net/http"
)

func TestAddRoute(t *testing.T) {
	router := NewEngine()
	router.addRoute("GET", "/", HandlerChain{},
		http.HandlerFunc(func(http.ResponseWriter, *http.Request) {}))

	assert.Len(t, router.trees, 1)
	assert.NotNil(t, router.trees.get("GET"))
	assert.Nil(t, router.trees.get("POST"))

	router.addRoute("POST", "/", HandlerChain{},
		http.HandlerFunc(func(http.ResponseWriter, *http.Request) {}))
	assert.Len(t, router.trees, 2)
	assert.NotNil(t, router.trees.get("GET"))
	assert.NotNil(t, router.trees.get("POST"))

	router.addRoute("POST", "/post", HandlerChain{},
		http.HandlerFunc(func(http.ResponseWriter, *http.Request) {}))
	assert.Len(t, router.trees, 2)
}

func TestAddRouteFails(t *testing.T) {
	router := NewEngine()
	assert.Panics(t, func() {
		router.addRoute("", "/", HandlerChain{},
			http.HandlerFunc(func(http.ResponseWriter, *http.Request) {}))
	})
	assert.Panics(t, func() {
		router.addRoute("GET", "a", HandlerChain{},
			http.HandlerFunc(func(http.ResponseWriter, *http.Request) {}))
	})
	assert.Panics(t, func() { router.addRoute("GET", "/", HandlerChain{}, nil) })

	router.addRoute("POST", "/post", HandlerChain{},
		http.HandlerFunc(func(http.ResponseWriter, *http.Request) {}))
	assert.Panics(t, func() {
		router.addRoute("POST", "/post", HandlerChain{},
			http.HandlerFunc(func(http.ResponseWriter, *http.Request) {}))
	})
}
