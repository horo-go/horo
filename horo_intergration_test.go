package horo

import (
	"testing"
	"net/http"
	"gitlab.com/lattetalk/lattetalk/vendor/github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	"time"
	"fmt"
)

func testRequest(t *testing.T, url string) {
	resp, err := http.Get(url)
	assert.NoError(t, err)
	defer resp.Body.Close()

	body, ioerr := ioutil.ReadAll(resp.Body)
	assert.NoError(t, ioerr)
	assert.Equal(t, "it worked", string(body), "resp body should match")
	assert.Equal(t, "200 OK", resp.Status, "should get a 200")
}


func TestEngine_RunEmpty(t *testing.T) {
	os.Setenv("PORT", "")
	router := NewEngine()
	go func() {
		router.GET("/example", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
			fmt.Fprintf(w, "it worked")
		})
		assert.NoError(t, router.Run())
	}()
	// have to wait for the go routine to start and run the server
	// otherwise the main thread will complete
	time.Sleep(5 * time.Millisecond)

	assert.Error(t, router.Run(":8080"))
	testRequest(t, "http://localhost:8080/example")
}

func TestEngine_RunWithEnv(t *testing.T) {
	os.Setenv("PORT", "3123")
	router := NewEngine()
	go func() {
		router.GET("/example", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
			fmt.Fprintf(w, "it worked")
		})
		assert.NoError(t, router.Run())
	}()
	// have to wait for the go routine to start and run the server
	// otherwise the main thread will complete
	time.Sleep(5 * time.Millisecond)

	assert.Error(t, router.Run(":3123"))
	testRequest(t, "http://localhost:3123/example")
}

func TestEngine_RunTooMuchParams(t *testing.T) {
	router := NewEngine()
	assert.Panics(t, func() {
		router.Run("2", "2")
	})
}
