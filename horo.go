package horo

import (
	"context"
	"log"
	"net/http"
	"os"
)

type contextKey string

var (
	default404Body = "404 page not found"
	default405Body = "405 method not allowed"
)

// Engine is the framework's instance, it contain router, middleware and configuration settings
// Create an instance of Engine by calling NewEngine()
type Engine struct {
	RouterGroup
	ErrorLog        *log.Logger
	cfg             *EngineConfig
	defaultNoRoute  http.Handler
	defaultNoMethod http.Handler
	noRouteHandler  http.Handler
	noMethodHandler http.Handler
	trees           methodTrees
}

type EngineConfig struct {
	// Enables automatic redirection if the current route can't be matched but a
	// handler for the path with (without) the trailing slash exists.
	// For example if /foo/ is requested but a route only exists for /foo, the
	// client is redirected to /foo with http status code 301 for GET requests
	// and 307 for all other request methods.
	RedirectTrailingSlash bool

	// If enabled, the router tries to fix the current request path, if no
	// handle is registered for it.
	// First superfluous path elements like ../ or // are removed.
	// Afterwards the router does a case-insensitive lookup of the cleaned path.
	// If a handle can be found for this route, the router makes a redirection
	// to the corrected path with status code 301 for GET requests and 307 for
	// all other request methods.
	// For example /FOO and /..//Foo could be redirected to /foo.
	// RedirectTrailingSlash is independent of this option.
	RedirectFixedPath bool

	// If enabled, the router checks if another method is allowed for the
	// current route, if the current request can not be routed.
	// If this is the case, the request is answered with 'Method Not Allowed'
	// and HTTP status code 405.
	// If no other Method is allowed, the request is delegated to the NotFound
	// handler.
	HandleMethodNotAllowed bool

	BasePathPrefix string
}

// NewEngine returns a new blank Engine instance without any middleware attached
func NewEngine(cfg *EngineConfig) *Engine {
	engine := &Engine{
		RouterGroup: RouterGroup{
			chain:    nil,
			basePath: cfg.BasePathPrefix,
			root:     true,
		},
		cfg:      cfg,
		ErrorLog: log.New(os.Stderr, "HORO  ", log.LstdFlags),
		trees:    make(methodTrees, 0, 9),
	}
	engine.RouterGroup.engine = engine
	engine.defaultNoRoute = http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		http.Error(w, default404Body, http.StatusNotFound)
	})
	engine.defaultNoMethod = http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		http.Error(w, default405Body, http.StatusInternalServerError)
	})
	engine.noRouteHandler = engine.defaultNoRoute
	engine.noMethodHandler = engine.defaultNoMethod
	return engine
}

func DefaultEngineConfig() *EngineConfig {
	return &EngineConfig{
		RedirectTrailingSlash:  true,
		RedirectFixedPath:      false,
		HandleMethodNotAllowed: false,
		BasePathPrefix:         "/",
	}
}

func (engine *Engine) logf(format string, args ...interface{}) {
	if engine.ErrorLog != nil {
		engine.ErrorLog.Printf(format, args...)
	} else {
		log.Printf(format, args...)
	}
}

// Use attaches a global middleware to the router. ie. the middleware attached though Use() will be
// included in the handlers chain for every single request. Even 404, 405, static files...
// For example, this is the right place for a logger or error management middleware.
func (engine *Engine) Use(middleware ...Handler) Route {
	engine.RouterGroup.Use(middleware...)
	engine.rebuild404Handler()
	engine.rebuild405Handler()
	return engine
}

func (engine *Engine) rebuild404Handler() {
	handler := engine.defaultNoRoute

	for i := range engine.chain {
		handler = engine.chain[len(engine.chain)-1-i](handler)
	}

	engine.noRouteHandler = handler
}

func (engine *Engine) rebuild405Handler() {
	handler := engine.defaultNoMethod

	for i := range engine.chain {
		handler = engine.chain[len(engine.chain)-1-i](handler)
	}

	engine.noMethodHandler = handler
}

func (engine *Engine) addRoute(method, path string, chain HandlerChain, handler http.Handler) {
	assert1(path[0] == '/', "path must begin with '/'")
	assert1(method != "", "HTTP method can not be empty")
	assert1(handler != nil, "handler cannot be nil")

	engine.logf("Mapping %-6s %-20s --> %s\n", method, path, nameOfFunction(handler))

	for i := range chain {
		handler = chain[len(chain)-1-i](handler)
	}

	root := engine.trees.get(method)
	if root == nil {
		root = new(node)
		engine.trees = append(engine.trees, methodTree{method: method, root: root})
	}
	root.addRoute(path, handler)
}

var (
	ParamsKey = contextKey("params-key")
)

// ServeHTTP conforms to serve http.Handler interface
func (engine *Engine) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path
	t := engine.trees
	for i, tl := 0, len(t); i < tl; i++ {
		if t[i].method == r.Method {
			root := t[i].root
			if handler, params, tsr := root.getValue(path); handler != nil {
				ctx := context.WithValue(r.Context(), ParamsKey, params)
				r = r.WithContext(ctx)
				handler.ServeHTTP(w, r)
				return
			} else if r.Method != "CONNECT" && path != "/" {
				code := 301 // Permanent redirect, request with GET method
				if r.Method != "GET" {
					// Temporary redirect, request with same method
					// As of Go 1.3, Go does not support status code 308.
					code = 307
				}

				if tsr && engine.cfg.RedirectTrailingSlash {
					if len(path) > 1 && path[len(path)-1] == '/' {
						r.URL.Path = path[:len(path)-1]
					} else {
						r.URL.Path = path + "/"
					}
					http.Redirect(w, r, r.URL.String(), code)
					return
				}

				// Try to fix the request path
				if engine.cfg.RedirectFixedPath {
					fixedPath, found := root.findCaseInsensitivePath(
						cleanPath(path),
						engine.cfg.RedirectTrailingSlash,
					)
					if found {
						r.URL.Path = string(fixedPath)
						http.Redirect(w, r, r.URL.String(), code)
						return
					}
				}
			}
			break
		}
	}

	if engine.cfg.HandleMethodNotAllowed {
		for _, tree := range engine.trees {
			if tree.method != r.Method {
				if handler, _, _ := tree.root.getValue(path); handler != nil {
					engine.noMethodHandler.ServeHTTP(w, r)
					return
				}
			}
		}
	}

	engine.noRouteHandler.ServeHTTP(w, r)
}
