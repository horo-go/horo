package horo

import (
	"net/http"
)

type ContextHandleFunc func(*Context)

type Handler func(http.Handler) http.Handler
type HandlerChain []Handler

func (f ContextHandleFunc) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	f(&Context{w, r})
}

// Param is a single URL parameter, consisting of a key and a value
type Param struct {
	Key   string
	Value interface{}
}

// Params is a Param-slice, as returned by the router.
// The slice is ordered, the first URL parameter is also the first slice value.
// It is therefore safe to read values by the index.
type Params []Param

func (p Params) Get(key string) (interface{}, int) {
	for i, param := range p {
		if param.Key == key {
			return param.Value, i
		}
	}
	return nil, -1
}

func (p Params) Put(index int, value interface{}) {
	p[index].Value = value
}


type H map[string]interface{}
