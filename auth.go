package horo

var AuthKey = contextKey("auth-key")

type Authentication struct {
	AccessToken   string
	Authenticated bool
	Subject       string
	Authorities   []string
}

func (au *Authentication) HasAuthority(authority string) bool {
	for _, v := range au.Authorities {
		if v == authority {
			return true
		}
	}
	return false
}